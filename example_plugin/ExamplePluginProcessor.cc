
#include "ExamplePluginProcessor.h"
#include <JANA/Services/JServiceLocator.h>
#include <ejana/MainOutputRootFile.h>
#include <MinimalistModel/McGeneratedParticle.h>
#include <fmt/core.h>

ExamplePluginProcessor::ExamplePluginProcessor() {
    SetTypeName(NAME_OF_THIS); // Provide JANA with this class's name
}

void ExamplePluginProcessor::Init() {
    auto root_file = GetApplication()->GetService<ej::MainOutputRootFile>();
    fmt::print("hello world\n");
    // Open TFiles, set up TTree branches, etc
}

void ExamplePluginProcessor::Process(const std::shared_ptr<const JEvent> &event) {
    LOG << "ExamplePluginProcessor::Process, Event #" << event->GetEventNumber() << LOG_END;
    
    /// Do everything we can in parallel
    /// Warning: We are only allowed to use local variables and `event` here
    auto particles = event->Get<minimodel::McGeneratedParticle>();

    /// Lock mutex
    std::lock_guard<std::mutex>lock(m_mutex);

    /// Do the rest sequentially
    /// Now we are free to access shared state such as m_heatmap
    //for (auto particle: particles) {
        /// Update shared state
    //}
}

void ExamplePluginProcessor::Finish() {
    // Close any resources
    LOG << "ExamplePluginProcessor::Finish" << LOG_END;
}

