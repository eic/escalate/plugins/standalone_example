
#ifndef _ExamplePluginProcessor_h_
#define _ExamplePluginProcessor_h_

#include <JANA/JEventProcessor.h>

class ExamplePluginProcessor : public JEventProcessor {

    // Shared state (e.g. histograms, TTrees, TFiles) live
    std::mutex m_mutex;
    
public:

    ExamplePluginProcessor();
    ~ExamplePluginProcessor() override = default;

    void Init() override;
    void Process(const std::shared_ptr<const JEvent>& event) override;
    void Finish() override;

};


#endif // _ExamplePluginProcessor_h_

