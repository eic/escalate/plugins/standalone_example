# Standalone plugin example

To run the example one can download 
[this beagle file: beagle_eD.txt](https://gitlab.com/eic/escalate/workspace/-/raw/master/data/beagle_eD.txt)

To run the plugin from command line:

```bash
ejana -Pplugin=beagle_reader,example_plugin -nthreads=1 beagle_eD.txt
```

To run python example